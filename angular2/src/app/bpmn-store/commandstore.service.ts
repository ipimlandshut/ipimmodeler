
export const COMMANDS = {
    SAVE: 'save',
    TWO_COLUMN: 'two-column',
    SET_IPIM_VALUES: 'set-ipim-values',
    SET_IPIM_VALUES_EVALUATE: 'set-ipim-values-evaluate',
    RESET: 'reset',
    SET_TERM: 'set-term',
    HIGHLIGHT: 'highlight',
    EXTRA: 'extra'
}