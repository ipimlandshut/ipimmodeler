# IntSys (Intelligente Systeme) 

Teilprojekt aus dem EFRE geförderten Projekt Intelligente Produktionssysteme

Ziel ist es durch Zugriff auf digitalisiertes Expertenwissen und einer (Teil-) Automatisierung von Routineaufgaben 
eine Entlastung für Logistikplaner herbeizuführen.


## Abhängigkeiten:

https://github.com/bpmn-io/bpmn-js (https://bpmn.io/)

bpmn-js is a BPMN 2.0 diagram rendering toolkit and web modeler.

## Weiterer Entwicklungsverlauf

Die derzeitige Version im Ordner jQuery nutzt das bpmn-js Tool der Firma Camunda. 
Dieses wird mit jQuery erweitert.

Die weitere Entwicklung wird unter Einsatz aktuellerer Tools geschehen. 
Eine Idee ist die Nutzung von TypeScript in Kombination mit React, um typsicheren Javascript Code zu schreiben und diesen in Komponenten zu gliedern.

## React
Benötigt:
    Yarn
    node-js

ps. Developing with MacOs would work like charm.. (thanks to windows...)